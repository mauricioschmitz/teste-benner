/**
Task
Write a class Network. The constructor should take a positive integer value indicating the number of elements in the set. Passing in an invalid value should throw an exception. The class should also provide two public methods, connect and query. 
The first method, connect will take two integers indicating the elements to connect. This method should throw exceptions as appropriate. 
The second method, query will also take two integers and should also throw an exception as appropriate. 
It should return true if the elements are connected, directly or indirectly, and false if the elements are not connected. The class can have as many private or protected members as needed for a good implementation.
 */
using System;
using System.Linq;
using System.Collections.Generic;

namespace benner
{
    //Abstract Class ConnectedElementsGroupAbstract
    abstract class ConnectedElementsGroupAbstract
    {
        /// <summary>
        /// Param list of ConnectedElementsGroup
        /// </summary>
        protected List<Object> elements = new List<Object>();

        public abstract void add(Pair pair);

        public abstract bool hasElement(int input);
        
        abstract public bool contains(Pair pair);
    }

    //Class ConnectedElementsGroup implementation
    class ConnectedElementsGroup : ConnectedElementsGroupAbstract
    {
        /// <summary>
        /// Construct method of class
        /// </summary>
        /// <param name="pair">Pair of elements</param>
        /// <returns>Void</returns>
        public ConnectedElementsGroup(Pair pair)
        {
            var pairElements = pair.getElements();
            this.elements.Add(pairElements.GetValue(0));
            this.elements.Add(pairElements.GetValue(1));
        }

        
        /// <summary>
        /// This method used to add a pair of elements to a group
        /// </summary>
        /// <param name="pair">Pair of elements</param>
        /// <returns>Void</returns>
       public override void add(Pair pair)
        {
            foreach (int element in this.elements) {
                if (pair.has(element)) {
                    this.elements.Add(pair.other(element));
                    break;
                }
            }
        }
        
        /// <summary>
        /// Method to search an integer in connectedElement group
        /// </summary>
        /// <param name="input">Element to search an integer in connectedElement group</param>
        /// <returns>boll</returns>
        public override bool hasElement(int input)
        {
            foreach (int element in this.elements) {
                if (input == element) {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Method to search a pair in connectedElement group
        /// </summary>
        /// <param name="input">Pair to search in connectedElement group</param>
        /// <returns>boll</returns>
        public override bool contains(Pair pair)
        {
            foreach (int element in this.elements) {
                if (pair.has(element)) {
                    return true;
                }
            }

            return false;
        }
    }

    //Pair class implementation
    class Pair
    {
        ///<summary>
        /// First integer of pair
        ///</summary>
        public int firstElement { get; set; }

        ///<summary>
        /// Second integer of pair
        ///</summary>
        public int secondElement { get; set; }

        /// <summary>
        /// Construct method of class
        /// </summary>
        /// <param name="firstElement">First integer of pair</param>
        /// <param name="secondElement">Second integer of pair</param>
        /// <returns>Void</returns>
        public Pair(int firstElement, int secondElement)
        {
            this.firstElement  = firstElement;
            this.secondElement = secondElement;
        }

        /// <summary>
        /// Method to check int element already in pair
        /// </summary>
        /// <param name="element">Element to check</param>
        /// <returns>bool</returns>
        public bool has(int element)
        {
            return this.firstElement == element || this.secondElement == element;
        }

        /// <summary>
        /// Method to get other element of pair
        /// </summary>
        /// <param name="element">Element used to find other</param>
        /// <returns>int</returns>
        public int other(int element)
        {
            if (this.firstElement == element) {
                return this.secondElement;
            }
            if (this.secondElement == element) {
                return this.firstElement;
            }
            //Dispatch Exception if element not exists used to find other
            throw new ArgumentException("Wrong element passed. Bug in Pair.other()");
        }

        /// <summary>
        /// Method to return an array of elements of pair
        /// </summary>
        /// <returns>Array</returns>
        public Array getElements()
        {
            int[] elements = {this.firstElement, this.secondElement};
            return elements;
        }
    }

    //Static class implementation used to Verify and validate values
    static class Verifier
    {
        /// <summary>
        /// Method to check if max number is a postive integer
        /// </summary>
        /// <param name="max">Element to of max values</param>
        /// <returns>void</returns>
        public static void checkPositive(int max)
        {
            if (0 >= (int)max) {
                throw new ArgumentOutOfRangeException();
            }
        }

        /// <summary>
        /// Method to verify parameters send is a valid set.
        /// </summary>
        /// <param name="max">Element to of max values</param>
        /// <param name="firstElement">First element of pair</param>
        /// <param name="secondElement">Second element of pair</param>
        /// <returns>void</returns>
        public static void verify(int max, int firstElement, int secondElement)
        {
            //Check if firstElement is positive
            if (0 >= firstElement) {
                throw new ArgumentOutOfRangeException("firstElement parameter must be positive");
            }

            //Check if max number is less then firstElement
            if (max < firstElement) {
                throw new ArgumentOutOfRangeException("firstElement parameter must be below or equal max");
            }

            //Check if secondElement is positive
            if (0 >= secondElement) {
                throw new ArgumentOutOfRangeException("secondElement parameter must be positive");
            }

            //Check if max number is less then secondElement
            if (max < secondElement) {
                throw new ArgumentOutOfRangeException("secondElement parameter must be below or equal max");
            }
        }

    }
    
    // Interfave of Network implementation
    interface NetworkInterface
    {
        void connect(int firstElement, int secondElement);

        bool query(int firstElement, int secondElement);
    }

    //Abstract Network Class implementation
    abstract class NetworkAbstract : NetworkInterface
    {
        ///<summary>
        /// Integer of total set of elements
        ///</summary>
        protected int total { get; set; }

        ///<summary>
        /// List of ConnectedElementsGroup
        ///</summary>
        protected List<ConnectedElementsGroup> groups { get; set; }

        /// <summary>
        /// Method to make a connection of 2 elements
        /// </summary>
        /// <param name="firstElement">First element of pair</param>
        /// <param name="secondElement">Second element of pair</param>
        /// <returns>void</returns>
        abstract public void connect(int firstElement, int secondElement);

        /// <summary>
        /// Method to check if two elements area connected
        /// </summary>
        /// <param name="firstElement">First element of pair</param>
        /// <param name="secondElement">Second element of pair</param>
        /// <returns>bool</returns>
        abstract public bool query(int firstElement, int secondElement);
    }
    class Network : NetworkAbstract
    {
        /// <summary>
        /// Constructo Method
        /// </summary>
        /// <param name="total">Number of elements of valid set</param>
        /// <returns>void</returns>
        public Network(int total)
        {
            try
            {
                List<ConnectedElementsGroup> list = new List<ConnectedElementsGroup>();
                this.groups = list;
                
                //Call function to verify if total is a positive integer
                Verifier.checkPositive(total);
                
                this.total = total;
            }
            catch (ArgumentOutOfRangeException)
            {
                Console.WriteLine("O total não é um inteiro positivo");
            }
            
        }

        /// <summary>
        /// Method to make a connection of 2 elements
        /// </summary>
        /// <param name="firstElement">First element of pair</param>
        /// <param name="secondElement">Second element of pair</param>
        /// <returns>void</returns>
        public override void connect(int firstElement, int secondElement)
        {
            //Call method to verify if arguments is valids
            Verifier.verify(this.total, firstElement, secondElement);

            var pair = new Pair(firstElement, secondElement);
            //Iterate Pair groups and effective the connection
            if(this.groups.Count > 0){
                foreach(ConnectedElementsGroup group in this.groups)
                {
                    if(group.contains(pair))
                    {
                        group.add(pair);
                    }
                }
            }
            //
            this.groups.Add(new ConnectedElementsGroup(pair));
            
        }

        /// <summary>
        /// Method to check if two elements area connected
        /// </summary>
        /// <param name="firstElement">First element of pair</param>
        /// <param name="secondElement">Second element of pair</param>
        /// <returns>bool</returns>
        public override bool query(int firstElement, int secondElement)
        {
            Verifier.verify(this.total, firstElement, secondElement);

            var pair = new Pair(firstElement, secondElement);
            var pairElements = pair.getElements();

            // Console.WriteLine(pairElements.GetValue(0));

            bool[] results = {false, false};

            if (this.groups.Count == 0) {
                return false;
            }

            foreach (ConnectedElementsGroup group in this.groups) {
                int el = (int)pairElements.GetValue(0);
                if (group.hasElement(el)) {
                    results[0] = true;
                }

                el = (int)pairElements.GetValue(1);
                if (group.hasElement(el)) {
                    results[1] = true;
                }
            }

            return results[0] && results[1];
        }
    }
}