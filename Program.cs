﻿using System;

namespace benner
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Connections!");

            var network = new Network(10);
            //Make any connections
            network.connect(1, 4);
            network.connect(4, 7);
            network.connect(1, 4);
            //And try check if is connected
            Console.WriteLine(network.query(1, 7));
            Console.WriteLine(network.query(1, 10));
            //Add tested connection
            network.connect(1, 10);
            //Retest connection
            Console.WriteLine(network.query(1, 10));
        }
    }
}
